package hw1Lesson13;

import java.util.Scanner;

public class PhoneNumber {
    public static void main(String[] args) {
        System.out.println("Enter phone number");
        Scanner input = new Scanner(System.in);
        String phone;

        do {
            phone = input.nextLine();
            if (phoneCheck(phone)) {
                System.out.println("Phone number validation succeeded!");
            } else {
                System.out.println("Phone number validation failed! Please, try again.");
            }
        } while (!phoneCheck(phone));

    }

    private static boolean phoneCheck(String s) {

        String pattern = "^(\\s*)?([- ()+]?\\d){10,14}";
        return s.matches(pattern);
    }

}