package hw2Lesson14.task1;

public class Circle extends Shape {

    private final double pi = Math.PI;
    private double radius;

    Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculatePerimeter() {
        return pi * (radius * 2);
    }

    @Override
    public double calculateArea() {
        return pi * (radius * radius);
    }
}
