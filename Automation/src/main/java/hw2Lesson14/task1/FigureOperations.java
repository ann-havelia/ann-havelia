package hw2Lesson14.task1;

import java.util.Scanner;

class FigureOperations {

  static void rectangleCalculation() {
    Scanner input = new Scanner(System.in);
    System.out.println("Enter width");
    double width = input.nextDouble();
    System.out.println("Enter length");
    double length = input.nextDouble();
    Rectangle rect = new Rectangle(width, length);
    System.out.println("The perimeter is: " + rect.calculatePerimeter() + ", the area is: " + rect.calculateArea());

    triangleSideCalculation(width, length);
  }

  static void squareCalculation() {
    Scanner input = new Scanner(System.in);
    System.out.println("Enter square side's length");
    double side = input.nextDouble();
    Square square = new Square(side);
    System.out.println("The perimeter is: " + square.calculatePerimeter() + ", the area is: " + square.calculateArea());
    triangleSideCalculation(side, side);
  }

  static void triangleCalculation() {
    Scanner input = new Scanner(System.in);
    System.out.println("Enter side A");
    double a = input.nextDouble();
    System.out.println("Enter side B");
    double b = input.nextDouble();
    System.out.println("Enter side C");
    double c = input.nextDouble();
    Triangle triangle = new Triangle(a, b, c);
    System.out.println("The perimeter is: " + triangle.calculatePerimeter() + ", the area is: " + triangle.calculateArea());
  }

  static void circleCalculation() {
    Scanner input = new Scanner(System.in);
    System.out.println("Enter the radius of the circle");
    double radius = input.nextDouble();
    Circle circle = new Circle(radius);
    System.out.println("The perimeter is: " + circle.calculatePerimeter() + ", the area is: " + circle.calculateArea());
  }

  private static void triangleSideCalculation(double a, double b) {
    double sqrC = (a * a) + (b * b);
    double c = Math.sqrt(sqrC);
    System.out.format("The side A of the triangle equals to " + a + ", side B equals to " + b + " and side C equals to %.1f%n", c);
  }
}