package hw2Lesson14.task1;

import java.util.Scanner;

public class PerimeterAreaCalculation {

  public static void main(String[] args) {
    System.out.println("Enter a geometric figure name to perform the calculation.");
    Scanner input = new Scanner(System.in);
    String value = input.nextLine();
    System.out.println("You have entered: " + value);
    value = value.toUpperCase();

    try {
      Figure figure = Figure.valueOf(value);

      switch (figure) {
        case RECTANGLE:
          FigureOperations.rectangleCalculation();
          break;
        case SQUARE:
          FigureOperations.squareCalculation();
          break;
        case TRIANGLE:
          FigureOperations.triangleCalculation();
          break;
        case CIRCLE:
          FigureOperations.circleCalculation();
          break;
      }
    } catch (IllegalArgumentException ex) {
      System.out.println("You have entered an invalid figure");
    }
  }
}
