package hw2Lesson14.task1;

public class Rectangle extends Shape {
    private double width;
    private double length;

    Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (width + length);
    }

    @Override
    public double calculateArea() {
        return width * length;
    }
}
