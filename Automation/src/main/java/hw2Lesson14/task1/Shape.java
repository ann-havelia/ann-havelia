package hw2Lesson14.task1;

public abstract class Shape {
    abstract public double calculatePerimeter();

    abstract public double calculateArea();
}
