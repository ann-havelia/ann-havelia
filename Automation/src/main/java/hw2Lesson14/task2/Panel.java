package hw2Lesson14.task2;

import java.util.Scanner;

public class Panel {
  public static void main(String[] args) {

    while (true) {
      WashingMachine samsung = new WashingMachine();
      samsung.turnOn();
      String program = setProgram();
      samsung.setProgram(program);
      int spin = setSpin();
      samsung.setSpin(spin);
      int temperature = setTemperature();
      samsung.setTemperature(temperature);
      int seconds = setTimer();
      samsung.setTimer(seconds);
      samsung.startWashing();
      samsung.endWashing();
    }
  }
  private static String setProgram() {
    String program;
    do {
      programOutput();
      Scanner input = new Scanner(System.in);
      program = input.nextLine();
    } while (!WashingType.validProgram(program.toUpperCase()));
    return program;
  }

  private static int setTemperature() {
    int temperature;
    do {
      temperatureOutput();
      Scanner input = new Scanner(System.in);
      temperature = input.nextInt();
    }
    while (!Temperature.validTemperature(temperature));
    return temperature;
  }

  private static int setSpin() {
    int spin;
    do {
      spinOutput();
      Scanner input = new Scanner(System.in);
      spin = input.nextInt();
    }
    while (!SpinSpeed.validSpin(spin));
    return spin;
  }

  private static int setTimer() {
    System.out.println("Please, set washing time in seconds");
    Scanner input = new Scanner(System.in);
    return input.nextInt();
  }

  private static void programOutput() {
    System.out.println("Select a program from the following: ");
    for (WashingType type : WashingType.values()) {
      System.out.println(type);
    }
  }

  private static void temperatureOutput() {
    System.out.println("Please, set a temperature from the following: ");
    for (Temperature temp : Temperature.values()) {
      System.out.println(temp.getTemperature());
    }
  }

  private static void spinOutput() {
    System.out.println("Select a spin speed from the following: ");
    for (SpinSpeed speed : SpinSpeed.values()) {
      System.out.println(speed.getSpin());
    }
  }

}
