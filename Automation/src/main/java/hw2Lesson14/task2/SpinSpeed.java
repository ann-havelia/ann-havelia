package hw2Lesson14.task2;

public enum SpinSpeed {
  SPIN400(400), SPIN800(800), SPIN1200(1200), SPIN1400(1400);

  private int spin;

  SpinSpeed(int spin) {
    this.spin = spin;
  }

  static public Boolean validSpin(int g) {
    SpinSpeed[] values = SpinSpeed.values();
    for (SpinSpeed value : values) {
      if (value.spin == g)
        return true;
    }
    return false;
  }

  public int getSpin() {
    return spin;
  }

}
