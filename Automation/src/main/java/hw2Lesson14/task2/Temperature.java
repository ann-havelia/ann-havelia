package hw2Lesson14.task2;

public enum Temperature {
  TEMP30(30), TEMP40(40), TEMP60(60), TEMP90(90);

  private int temperature;

  Temperature(int temperature) {
    this.temperature = temperature;
  }

  static public Boolean validTemperature(int g) {
    Temperature[] values = Temperature.values();
    for (Temperature value : values) {
      if (value.temperature == g)
        return true;
    }
    return false;
  }

  public int getTemperature() {
    return temperature;
  }
}
