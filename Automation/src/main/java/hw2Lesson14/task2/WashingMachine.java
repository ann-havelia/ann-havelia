package hw2Lesson14.task2;

public class WashingMachine {

  private String program;
  private int spin;
  private int temperature;
  private long time;


  void setProgram(String program) {
    this.program = program;
  }

  void setSpin(int spin) {
    this.spin = spin;
  }

  void setTemperature(int temperature) {
    this.temperature = temperature;
  }

  void setTimer(long seconds) {
    this.time=seconds*1000;
  }

  public void turnOn() {
    System.out.println("Please, select a program");
  }

  void startWashing() {
    System.out.println("Closed, the washing has started with program "+ program + " with temperature "+temperature+" with rotation speed "+spin );
    try{
    Thread.sleep(time);}
    catch (InterruptedException e){
      System.out.println(e);
    }
  }
  public void endWashing() {
    System.out.println("Washing is finished.");
  }
}