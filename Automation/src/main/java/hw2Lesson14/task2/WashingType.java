package hw2Lesson14.task2;

public enum WashingType {
  COTTON, SYNTHETICS, WOOL, EVERYDAY, DELICATE;


  static public Boolean validProgram(String g) {
    WashingType[] values = WashingType.values();
    for (WashingType value : values) {
      if (value.name().equals(g))
        return true;
    }
    return false;
  }
}
