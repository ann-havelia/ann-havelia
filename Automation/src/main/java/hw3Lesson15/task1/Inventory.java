package hw3Lesson15.task1;


import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Inventory implements InventoryInterface {

    private List<Product> inventory = new ArrayList<>();

    @Override
    public boolean addProduct(Scanner input) {
        String name;
        float price = 0;
        int quantity = 0;

        System.out.println("Enter product name");
        Scanner s = new Scanner(System.in);
        name = s.nextLine();
        try {
            System.out.println("Enter price of the product");
            price = s.nextInt();
        } catch (InputMismatchException n) {
            System.out.println("You have entered invalid price value");
            return false;
        }
        try {
            System.out.println("Enter quantity");
            quantity = s.nextInt();
        } catch (InputMismatchException n) {
            System.out.println("You have entered invalid quantity value.");
            return false;
        }
        Product product = new Product(name, price, quantity);
        inventory.add(product);
        return true;
    }

    @Override
    public void printProductList() {
        int number = 0;
        for (Product product : inventory) {
            number++;
            System.out.println(number + "." + product.getName() + ", quantity: " + product.getQuantity() + ", price = " + product.getPrice());
        }
    }

    @Override
    public void printSumOfAllProducts() {
        double sum = 0;
        for (Product product : inventory) {
            double values = product.getPrice() * product.getQuantity();
            sum += values;
        }
        System.out.println("The total sum of all products in the inventory is: " + sum);
    }

    public void isEmpty() {
        if (inventory.isEmpty())
            System.out.println("The inventory is empty");
    }

    public void addProduct(Product product) {
        inventory.add(product);
    }

}
