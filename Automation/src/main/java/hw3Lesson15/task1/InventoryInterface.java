package hw3Lesson15.task1;

import java.util.Scanner;

public interface InventoryInterface {
  boolean addProduct(Scanner input);

  void printProductList();

  void printSumOfAllProducts();
}
