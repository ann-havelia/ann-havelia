package hw3Lesson15.task1;

import java.util.Scanner;

public class InventoryManager {
    private Inventory inventory = new Inventory();

    public static void main(String[] args) {
        Product p1 = new Product("TV", 1299, 25);
        Product p2 = new Product("Electric Grill", 299, 14);
        Product p3 = new Product("Headset", 99, 54);
        InventoryManager manager = new InventoryManager();
        manager.inventory.addProduct(p1);
        manager.inventory.addProduct(p2);
        manager.inventory.addProduct(p3);

        menu();
        Scanner option = new Scanner(System.in);
        while (manager.manageStock(option)) {
            menu();
        }

    }

    private static void menu() {
        System.out.println("\nChoose from the following options:" +
                "\n1. Add product" +
                "\n2. Show inventory" +
                "\n3. Print sum off all products" +
                "\n4. Exit ");
    }

    private boolean manageStock(Scanner input) {
        int option = Integer.parseInt(input.nextLine());

        switch (option) {
            case 1:
                inventory.addProduct(input);
                break;
            case 2:
                inventory.isEmpty();
                inventory.printProductList();
                break;
            case 3:
                inventory.printSumOfAllProducts();
                break;
            case 4:
                System.out.println("Closing app");
                System.exit(0);
        }
        return true;
    }
}
