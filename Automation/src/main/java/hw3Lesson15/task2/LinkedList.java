package hw3Lesson15.task2;

public class LinkedList<E> implements List<E> {

    private int size;
    private Node<E> head;

    @Override
    public void add(E element) {
        Node<E> node = new Node<>(element);
        if (head == null) {
            head = node;
        } else {
            Node<E> m = head;
            while (m.getNext() != null) {
                m = m.getNext();
            }
            m.setNext(node);
        }
    }

    @Override
    public boolean remove(E element) {
        if (head.getValue().equals(element)) {
            head = head.getNext();
        } else {
            Node<E> previous = head;
            Node<E> current = previous.getNext();
            while (current != null) {
                if (current.getValue().equals(element)) {
                    previous.setNext(current.getNext());
                    break;
                } else {
                    previous = current;
                    current = current.getNext();
                }
            }
        }
        return true;
    }

    @Override
    public void remove(int index) {
        if (head == null) {
            return;
        }
        Node<E> node = head;

        if (index == 0) {
            head = node.getNext();
            return;
        }
        for (int i = 1; i < index && node.getNext() != null; i++) {
            node = node.getNext();
        }
        Node<E> temp = node.getNext();

        if (temp == null) {
            System.out.println("You have entered index that out of bound of the list");
        } else {
            node.setNext(temp.getNext());
        }

    }

    @Override
    public E get(int index) {
        if (head == null) {
            return null;
        }
        Node<E> node = head;
        for (int i = 1; i <= index && node.getNext() != null; i++) {
            node = node.getNext();
        }
        Node<E> current = node;
        return current.getValue();
    }

    @Override
    public int size() {
        Node<E> m = head;
        int size = 0;

        while (m != null) {
            size++;
            m = m.getNext();
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public void print() {
        Node<E> m = head;
        while (m != null) {
            System.out.println(m.getValue());
            m = m.getNext();
        }
    }

}