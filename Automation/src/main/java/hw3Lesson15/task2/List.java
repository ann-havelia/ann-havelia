package hw3Lesson15.task2;

public interface List<E> {

    void add(E element);

    boolean remove(E element);

    void remove(int index);

    E get(int index);

    int size();

    boolean isEmpty();

    void print();
}