package hw3Lesson15.task2;

class Node<E> {

    private E value;
    private Node<E> next;

    Node(E value) {
        this.value = value;
    }

    E getValue() {
        return value;
    }

    Node<E> getNext() {
        return next;
    }

    void setNext(Node<E> next) {
        this.next = next;
    }
}