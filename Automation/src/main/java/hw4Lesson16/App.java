package hw4Lesson16;

import java.util.Scanner;

public class App {

    private TaskManagerIImpl taskManagerI = new TaskManagerIImpl();

    public static void main(String[] args) {
        App app = new App();
        menu();
        Scanner scanner = new Scanner(System.in);
        while (app.manageTask(scanner)) {
            menu();
        }
    }

    private static void menu() {
        System.out.println("\nHello" +
                "\nThis is the task manager application" +
                "\n1. Create task" +
                "\n2. Edit task" +
                "\n3. Remove task" +
                "\n4. Show all tasks" +
                "\n5. Exit  ");
    }

    private boolean manageTask(Scanner scanner) {
        int data = Integer.parseInt(scanner.nextLine());
        switch (data) {
            case 1:
                boolean result = taskManagerI.createTask(scanner);
                if (!result) {
                    return false;
                }
                break;
            case 2:
                taskManagerI.editTask(scanner);
                break;
            case 3:
                taskManagerI.removeTask(scanner);
                break;
            case 4:
                taskManagerI.showAllTasks();
                break;
            default:
                System.out.println("Closing application");
                System.exit(-1);
        }
        return true;
    }
}
