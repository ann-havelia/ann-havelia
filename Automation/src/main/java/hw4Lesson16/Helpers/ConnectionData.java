package hw4Lesson16.Helpers;

public class ConnectionData {


    public String getUrl() {
        String url = "jdbc:postgresql://127.0.0.1:5432/taskmanager";
        return url;
    }

    public String getUser() {
        String user = "postgres";
        return user;
    }

    public String getPass() {
        String pass = "postgres";
        return pass;
    }
}