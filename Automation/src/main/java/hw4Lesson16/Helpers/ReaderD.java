package hw4Lesson16.Helpers;

import java.sql.*;

public class ReaderD extends ConnectionData {
    public int getLastId() {
        final String SELECT_TASK_ID = "SELECT * FROM public.tasks";

        int identifier = 0;
        try (Connection connection = DriverManager
                .getConnection(getUrl(), getUser(), getPass());

             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TASK_ID, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
            System.out.println(preparedStatement);

            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            if (rs.next()) {
                rs.last();
                identifier = rs.getInt("id");
            }
            return identifier;

        } catch (SQLException e) {
            ErrorHandler.printSQLException(e);
        }
        return identifier;
    }

    public void getAllRecords() {
        // Step 1: Establishing a Connection
        try (Connection connection = DriverManager
                .getConnection(getUrl(), getUser(), getPass());

             Statement stmt = connection.createStatement()) {

            ResultSet rs = stmt.executeQuery("SELECT * FROM public.tasks;");

            while (rs.next()) {
                int identifier = rs.getInt("id");
                String firstName = rs.getString("title");
                String lastName = rs.getString("description");
                Date date = rs.getDate("date_updated");
                System.out.println(identifier + " - " + firstName + " " + lastName + " " + date);

                System.out.println();
            }
        } catch (SQLException e) {
            ErrorHandler.printSQLException(e);
        }
    }
}