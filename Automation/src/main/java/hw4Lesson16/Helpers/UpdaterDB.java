package hw4Lesson16.Helpers;

import java.sql.*;

public class UpdaterDB extends ConnectionData {
    public void updateTitleDesc(int id, String title, String description, Date date) {
        final String UPDATE_TASK = "UPDATE public.tasks SET title = ?, description = ?, date_updated = ? where id = ?;";

        // Step 1: Establishing a Connection
        try (Connection connection = DriverManager
                .getConnection(getUrl(), getUser(), getPass());

             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TASK);) {
            preparedStatement.setString(1, title);
            preparedStatement.setString(2, description);
            preparedStatement.setDate(3, date);
            preparedStatement.setInt(4, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            ErrorHandler.printSQLException(e);
        }
        // Step 4: try-with-resource statement will auto close the connection.
    }
}