package hw4Lesson16.Helpers;

import java.sql.*;

public class WriterDB extends ConnectionData {
    public void insertRecord(int id, String title, String description, Date date) throws SQLException {
        final String INSERT_TASK = "INSERT INTO public.tasks"
                +
                "  (id, title, description, date_updated) VALUES " +
                " (?, ?, ?, ?);";

        // Step 1: Establishing a Connection
        try (Connection connection = DriverManager
                .getConnection(getUrl(), getUser(), getPass());

             // Step 2:Create a statement using connection object


             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TASK)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, title);
            preparedStatement.setString(3, description);
            preparedStatement.setDate(4, date);

            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            preparedStatement.executeUpdate();

        } catch (SQLException e) {

            // print SQL exception information
            ErrorHandler.printSQLException(e);
        }

        // Step 4: try-with-resource statement will auto close the connection.
    }
}