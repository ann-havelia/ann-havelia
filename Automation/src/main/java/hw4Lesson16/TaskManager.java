package hw4Lesson16;

import java.util.Scanner;

public interface TaskManager {
    boolean createTask(Scanner scanner);

    void editTask(Scanner scanner);

    void removeTask(Scanner scanner);

    void showAllTasks();
}
