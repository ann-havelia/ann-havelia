package hw4Lesson16;

import hw4Lesson16.Helpers.ReaderDB;
import hw4Lesson16.Helpers.RemoverDB;
import hw4Lesson16.Helpers.UpdaterDB;
import hw4Lesson16.Helpers.WriterDB;

import java.sql.SQLException;
import java.util.Date;
import java.util.Scanner;

public class TaskManagerIImpl implements TaskManager {

    @Override
    public boolean createTask(Scanner scanner) {

        ReaderDB reader = new ReaderDB();
        WriterDB writer = new WriterDB();
        int id = reader.getLastId();
        id++;

        System.out.println("Enter title");
        String title = scanner.nextLine();

        System.out.println("Enter description");
        String description = scanner.nextLine();

        Date date = new Date();
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        try {
            writer.insertRecord(id, title, description, sqlDate);
        } catch (SQLException s) {
            System.out.println("Error writing to the database");
        }
        return true;
    }

    @Override
    public void editTask(Scanner scanner) {
        ReaderDB reader = new ReaderDB();
        UpdaterDB updater = new UpdaterDB();
        System.out.println("Enter task id");
        int id = Integer.parseInt(scanner.nextLine());
        while (!reader.checkTaskIdExists(id)) {
            System.out.println("The entered id is not present in the table.Please, Enter valid id.");
            id = Integer.parseInt(scanner.nextLine());
        }
        System.out.println("Enter title");
        String title = scanner.nextLine();

        System.out.println("Enter description");
        String description = scanner.nextLine();

        Date date = new Date();
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        updater.updateTitleDesc(id, title, description, sqlDate);
        System.out.println("The task with id" + id + "is successfully updated.");
    }

    @Override
    public void removeTask(Scanner scanner) {
        ReaderDB reader = new ReaderDB();
        RemoverDB remover = new RemoverDB();
        System.out.println("Enter task id to remove");
        int id = Integer.parseInt(scanner.nextLine());
        while (!reader.checkTaskIdExists(id)) {
            System.out.println("The entered id is not present in the table.Please, Enter valid id.");
            id = Integer.parseInt(scanner.nextLine());
        }
        remover.removeRecord(id);
        System.out.println("The entry is successfully deleted.");
    }

    @Override
    public void showAllTasks() {
        ReaderDB reader = new ReaderDB();
        reader.getAllRecords();
    }
}
