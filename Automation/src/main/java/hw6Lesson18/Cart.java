package hw6Lesson18;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private WebDriver driver;

    public Cart(WebDriver driver) {
        this.driver = driver;
    }

    public List<String> checkCart() {
        List<WebElement> cart = driver.findElements(By.className("product-name"));
        List<String> cartItems = new ArrayList<>();
        for (WebElement a : cart) {
            String[] s = a.getText().split(" ");
            cartItems.add(s[0]);
        }
        return cartItems;
    }

    public boolean promoCodeCheck(String promocode) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement code1 = driver.findElement(By.className("promoCode"));
        WebElement apply1 = driver.findElement(By.className("promoBtn"));
        code1.sendKeys(promocode);
        apply1.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("promoInfo")));
        return !driver.getPageSource().contains("Invalid code ..!");
    }

    public boolean completeOrder() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement completeOrder = driver.findElement(By.xpath("//*[text()='Place Order']"));
        completeOrder.click();
        Select country = new Select(driver.findElement(By.tagName("select")));
        country.selectByVisibleText("Ukraine");
        WebElement checkbox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("chkAgree")));
        checkbox.click();
        WebElement proceedBtn = driver.findElement(By.xpath("//*[text()='Proceed']"));
        proceedBtn.click();
        return driver.getPageSource().contains("Thank you, your order has been placed successfully");
    }

    public int checkCartTotal() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement cart = driver.findElement(By.className("cart-icon"));
        cart.click();
        WebElement proceed = driver.findElement(By.xpath("//*[text()='PROCEED TO CHECKOUT']"));
        proceed.click();
        WebElement total = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("totAmt")));
        return Integer.parseInt(total.getText());
    }
}
