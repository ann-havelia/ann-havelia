package hw6Lesson18;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Homepage {

    private WebDriver driver;


    public Homepage(WebDriver driver) {
        this.driver = driver;
    }

    public List<String> getAllProducts() {
        List<WebElement> products = driver.findElements(By.className("product-name"));
        List<String> names = new ArrayList<>();
        for (WebElement a : products) {
            String[] s = a.getText().split(" ");
            names.add(s[0]);
        }
        return names;
    }

    public void addSomeVeggiesToCart() {
        WebElement addCarrot = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/div/div[5]/div[3]/button"));
        addCarrot.click();
        WebElement addBroccoli = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/div/div[1]/div[3]/button"));
        addBroccoli.click();
    }

    public List<String> checkCartDropdown() {
        WebElement cartDropdown = driver.findElement(By.className("cart-icon"));
        cartDropdown.click();
        List<WebElement> cart = driver.findElements(By.className("product-name"));
        List<String> cartItems = new ArrayList<>();
        for (WebElement a : cart) {
            String[] s = a.getText().split(" ");
            cartItems.add(s[0]);
        }
        return cartItems;
    }

    public Cart checkout(){
        WebElement cart = driver.findElement(By.className("cart-icon"));
        cart.click();
        WebElement proceed = driver.findElement(By.xpath("//*[text()='PROCEED TO CHECKOUT']"));
        proceed.click();
        WebDriverWait wait = new WebDriverWait(driver, 6);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("promoCode")));
        return new Cart(driver);
    }

    public String invalidSearch(String searchRequest) {
        WebElement searchField = driver.findElement(By.className("search-keyword"));
        searchField.click();
        searchField.sendKeys(searchRequest);
        WebElement actualMessage = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[1]/div/div/h2"));
        return actualMessage.getText();
    }
}