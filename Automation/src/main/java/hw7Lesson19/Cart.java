package hw7Lesson19;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Cart {

    public List<String> checkCart() {
        ElementsCollection cart = $$(By.xpath("//p[@class='product-name']"));
        List<String> cartItems = new ArrayList<>();
        for (SelenideElement a : cart) {
            String[] s = a.getText().split(" ");
            cartItems.add(s[0]);
        }
        return cartItems;
    }

    public boolean promoCodeCheck(String promocode) {
        $(By.xpath("//input[@class='promoCode']")).setValue(promocode);
        $(By.xpath("//button[@class='promoBtn']")).click();
        SelenideElement info = $(By.xpath("//span[@class='promoInfo']")).should(Condition.exist);
        return !info.is(Condition.visible);
    }


    public boolean completeOrder() {
        $(By.xpath("//*[text()='Place Order']")).click();
        $(By.tagName("select")).selectOptionByValue("Ukraine");
        $(By.xpath("//input[@type='checkbox']")).click();
        $(By.xpath("//*[text()='Proceed']")).click();
        SelenideElement success = $(byText("Thank you, your order has been placed successfully")).should(Condition.exist);
        return success.isDisplayed();
    }

    public int checkCartTotal() {
        SelenideElement total = $(By.xpath("//span[@class='totAmt']"));
        return Integer.parseInt(total.getText());
    }
}
