package hw7Lesson19;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

import java.util.ArrayList;
import java.util.List;

public class Homepage {
    ElementsCollection products = $$(By.xpath("//h4[@class='product-name']"));
    ElementsCollection addProductButtons = $$(By.xpath("//button[@class='']"));

    public List<String> getAllProducts() {
        List<String> names = new ArrayList<>();
        for (SelenideElement a : products) {
            String[] s = a.getText().split(" ");
            names.add(s[0]);
        }
        return names;
    }

    public void addItemToCart(String item) {
        List<String> products = getAllProducts();
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).equals(item)) {
                addProductButtons.get(i).click();
            }
        }
    }

    public List<String> checkCartDropdown() {
        $(By.xpath("//a[@class='cart-icon']")).click();
        ElementsCollection items = $$(By.xpath("//p[@class='product-name']"));
        List<String> cartItems = new ArrayList<>();
        for (SelenideElement a : items) {
            String[] s = a.getText().split(" ");
            cartItems.add(s[0]);
        }
        return cartItems;
    }

    public Cart checkout() {
        $(By.xpath("//a[@class='cart-icon']")).waitUntil(Condition.visible,5).click();
        $(byText("PROCEED TO CHECKOUT")).click();
        return page(Cart.class);
    }

    public String invalidSearch(String searchRequest) {
        $(By.className("search-keyword")).setValue(searchRequest);
        SelenideElement actualMessage = $(By.xpath("//div[@class='no-results']/descendant::h2"));
        return actualMessage.getText();
    }
}