package hw5Lesson17;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CompareValuesTest {

    @Test
    public void compareRandomInts() {
        Random random = new Random();
        int a = random.nextInt(100);
        int b = random.nextInt(100);
        Assert.assertEquals(b, a);
    }

    @Test
    public void compareTwoStrings() {
        String a = "Duck";
        String b = "Luck";
        Assert.assertNotEquals(b, a);
    }

    @Test
    public void findElemInArray() {
        int k = 25;
        List<Integer> array = Arrays.asList(28, 67, 3, 2, 66, 11, 25, 45, 21, 212, 10);
        Assert.assertTrue(array.contains(k));

    }

    @Test
    public void compareSameInts() {
        int a = 42;
        int b = 42;
        Assert.assertSame(a, b);
    }

    @Test
    public void compareSameStrings() {
        String c = "Lorem Ipsum";
        String d = "Dolorem";
        Assert.assertSame(c, d);
    }
}

