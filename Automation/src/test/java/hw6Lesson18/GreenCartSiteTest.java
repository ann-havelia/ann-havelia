package hw6Lesson18;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;


public class GreenCartSiteTest {

    public WebDriver driver;
    public String baseUrl =
            "https://rahulshettyacademy.com/seleniumPractise/#/";

    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "/Users/annhavelia/qa-training/webDriver/chromedriver");
        driver = new ChromeDriver();

    }

    @AfterTest
    public void tearDown() {
        driver.close();
    }

    @Test
    public void checkTitle() {
        driver.get(baseUrl);
        String expectedTitle = "GreenKart - veg and fruits kart";
        String actualTitle = driver.getTitle();
        Assert.assertEquals(expectedTitle, actualTitle);
    }

    @Test
    public void findAnyVeggies() {
        driver.get(baseUrl);
        Homepage page = new Homepage(driver);
        String capsicum = "Tomato";
        String carrot = "Carrot";
        List<String> products = page.getAllProducts();
        Assert.assertTrue(products.contains(capsicum));
        Assert.assertTrue(products.contains(carrot));
    }

    @Test
    public void addVeggiestoCart() {
        driver.get(baseUrl);
        Homepage page = new Homepage(driver);
        page.addSomeVeggiesToCart();
        Assert.assertTrue(page.checkCartDropdown().contains("Carrot"));
        Assert.assertTrue(page.checkCartDropdown().contains("Brocolli"));
    }

    @Test
    public void checkout() {
        driver.get(baseUrl);
        Homepage page = new Homepage(driver);
        Cart cart = new Cart(driver);
        page.addSomeVeggiesToCart();
        page.checkout();
        Assert.assertTrue(cart.checkCart().contains("Carrot"));
        Assert.assertTrue(cart.checkCart().contains("Brocolli"));
    }

    @Test
    public void checkWrongCode() {
        driver.get(baseUrl);
        Homepage page = new Homepage(driver);
        Cart cart = new Cart(driver);
        page.addSomeVeggiesToCart();
        page.checkout();
       Assert.assertFalse(cart.promoCodeCheck("DJHFB#C"));
    }

    @Test
    public void completeOrder() {
        driver.get(baseUrl);
        Homepage page = new Homepage(driver);
        Cart cart = new Cart(driver);
        page.addSomeVeggiesToCart();
        page.checkout();
        Assert.assertTrue(cart.completeOrder());

    }

    @Test
    public void checkInvalidSearch() {
        driver.get(baseUrl);
        Homepage page = new Homepage(driver);
        String searchRequest = "Carambol";
        String expectedMessage = "Sorry, no products matched your search!";
        Assert.assertEquals(page.invalidSearch(searchRequest), expectedMessage);
    }

    @Test
    public void checkTotalAmount(){
        driver.get(baseUrl);
        Homepage page = new Homepage(driver);
        Cart cart = new Cart(driver);
        page.addSomeVeggiesToCart();
        int total = 176;
        Assert.assertEquals(total, cart.checkCartTotal());
    }
}
