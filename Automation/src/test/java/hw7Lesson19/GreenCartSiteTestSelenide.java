package hw7Lesson19;

import com.codeborne.selenide.Configuration;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.List;
import static com.codeborne.selenide.Selenide.*;


public class GreenCartSiteTestSelenide {


    public String baseUrl =
            "https://rahulshettyacademy.com/seleniumPractise/#/";
    Homepage page = new Homepage();
    Cart cart = new Cart();

    @BeforeMethod
    public void setBrowser() {
        Configuration.browser = "chrome";
        Configuration.timeout = 6000;
    }

    @Test
    public void checkTitle() {
        open(baseUrl);
        Assert.assertEquals(title(), "GreenKart - veg and fruits kart");
    }

    @Test
    public void findAnyVeggies() {
        open(baseUrl);
        String tomato = "Tomato";
        String carrot = "Carrot";
        List<String> products = page.getAllProducts();
        Assert.assertTrue(products.contains(tomato));
        Assert.assertTrue(products.contains(carrot));
    }

    @Test(dependsOnMethods = "findAnyVeggies")
    public void addVeggiestoCart() {
        open(baseUrl);
        page.addItemToCart("Carrot");
        page.addItemToCart("Brocolli");
        List<String> cartItems = page.checkCartDropdown();
        Assert.assertTrue(cartItems.contains("Carrot"));
        Assert.assertTrue(cartItems.contains("Brocolli"));
    }

    @Test(dependsOnMethods = "addVeggiestoCart")
    public void checkout() {
        open(baseUrl);
        page.addItemToCart("Carrot");
        page.addItemToCart("Brocolli");
        page.checkout();
        Assert.assertTrue(cart.checkCart().contains("Carrot"));
        Assert.assertTrue(cart.checkCart().contains("Brocolli"));
    }

    @Test(dependsOnMethods = "addVeggiestoCart")
    public void checkWrongCode() {
        open(baseUrl);
        page.addItemToCart("Carrot");
        page.addItemToCart("Brocolli");
        page.checkout();
        Assert.assertFalse(cart.promoCodeCheck("DJHFB#C"), "Invalid code ..!");
    }

    @Test(dependsOnMethods = "checkout")
    public void completeOrder() {
        open(baseUrl);
        Assert.assertTrue(cart.completeOrder());
    }

    @Test(dependsOnMethods = {"addVeggiestoCart",})
    public void wrongSearch() {
        open(baseUrl);
        String searchRequest = "Carambol";
        String expectedMessage = "Sorry, no products matched your search!";
        Assert.assertEquals(page.invalidSearch(searchRequest), expectedMessage);
    }

    @Test(priority = 1)
    public void checkTotalAmount() {
        open(baseUrl);
        page.addItemToCart("Carrot");
        page.addItemToCart("Brocolli");
        page.checkout();
        Assert.assertEquals(176, cart.checkCartTotal());
    }
}
